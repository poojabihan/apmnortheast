//
//  Messages+CoreDataProperties.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 16/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "Messages+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Messages (CoreDataProperties)

+ (NSFetchRequest<Messages *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *lastmsg;
@property (nullable, nonatomic, copy) NSString *msgdetail;
@property (nullable, nonatomic, copy) NSString *msgtype;

@end

NS_ASSUME_NONNULL_END
