//
//  CandidateRegistrationVC.h
//  APMNorthEast
//
//  Created by Alok Mishra on 25/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CandidateRegistrationVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *feedback_btn;
@property (weak, nonatomic) IBOutlet UIButton *feedback_pop_btn;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *view4;
@property (weak, nonatomic) IBOutlet UIView *view5;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

- (IBAction)feedback_btn_click:(id)sender;
@property(nonatomic) BOOL  _isQuesFromNotify;
@end
