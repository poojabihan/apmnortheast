//
//  TwitterTimeLineViewController.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 16/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <TwitterKit/TwitterKit.h>

@interface TwitterTimeLineViewController : TWTRTimelineViewController

@end
